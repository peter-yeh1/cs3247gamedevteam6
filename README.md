# SOL LABS

## Teaser
Sol Labs is a first-person, 3D escape room puzzle game set in a futuristic robotics lab. You awake to the sound of a mechanical voice, and are greeted by its owner, a tiny drone. Explore the lab and solve puzzles with his help so that you can both escape. What secrets does he hold and what do the owners of this lab want with the both of you?

## Introduction
The player (a human) is trapped in an enclosed laboratory with his robotic companion, a drone. Both the human and the drone can be controlled in first person perspective, and the player can switch between the two characters. The human can walk around and pick up and inspect objects, while the drone can crawl and fly.

- Target Audience: General puzzle fans, children to adults
- Genre: First Person Puzzle Game
- Intended Platforms: PC (Windows and MacOS)

## Setting
Set in a futuristic robotics lab, scientists are trying to put human souls into robots. The drone is one of those experiments. You have been captured and will be used as a test subject. If you don’t escape in time, your soul will also be trapped in a robot. Initially the player does not know this — he discovers the truth as he escapes.

## Feature List
- First person perspective view
- Able to see the world from two different perspectives: the human’s and the drone’s
- Come up with creative ways to control the drone and use objects to solve puzzles
- Drone may be able to see different things as compared to the human, and that may help the player in clearing the level
- Tutorial Level
- Player can learn the controls
- Player can learn what the drone is capable of doing

## List of game mechanics and dynamics
### PLAYER:
Normal vision, first person perspective
Walk around
Able to hold items and interact with objects
Can combine various items together, for example:
A battery and a flashlight to activate a light-sensitive sensor

### DRONE:
Similar to Sova’s drone from valorant, in terms of how it moves
Special vision
Similar to UV vision
See hidden messages on walls/objects, or hidden objects
Controls to fly around, giving the drone a different perspective than the player
Small size which allows it to go through small openings/doors

### ENVIRONMENT/LEVEL PROGRESSION MECHANICS:
Buttons/Levers to be held down or pushed by the player or drone
Doors that restricts player/drone from passing through
Big fan/a wind tunnel
To transport items and restrict movement
Floor pad that needs to be stepped on/weighed down by other objects


## Key Technical Focus
- Changing perspectives and vision between the human and the drone
- Physics between the different interactable objects

